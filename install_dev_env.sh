#!/bin/bash
#==============================================================
#     INSTALLATION DE L'ENVIRONNEMENT DE DEV
#==============================================================

DEV_BUILDROOT=true
DEV_PCCN=true
DEV_ELECTRE=true

RABBIT_VCS=false
ECLIPSE=false

# Eclipse Oxygen 4.7
URL_ECLIPSE_32="https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/3a/eclipse-cpp-oxygen-3a-linux-gtk.tar.gz&mirror_id=1"
# Eclipse 2019-03
URL_ECLIPSE_64="https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2019-03/R/eclipse-cpp-2019-03-R-linux-gtk-x86_64.tar.gz&mirror_id=1039"


# Code couleur
green='\033[0;32m'
yellow='\033[0;33m'
nc='\033[0m' # No Color

# Update de la distrib
function update()
{
	echo ""
	echo -e "${green}Mise à jour ...${nc}"
	sudo apt-get update -qq
	sudo apt upgrade -y -qq
	echo -e "${green}Mise à jour Ok${nc}"
}


# Installation des outils de base
function intall_outils_dev()
{
	echo ""
	echo -e "${green}Installation Outils dev ...${nc}"

	sudo apt install -y \
		build-essential \
		git \
		meld \
		geany \
		htop \
		tree \
		terminator \
		dos2unix \
		gdb-multiarch \
		gparted \
		wget

	# check 32 ou 64 bits
	if [[ 'x86_64' == $(uname -m) ]]; then
		echo "Machine 64 bits : installation des libs 32 bits"
		sudo apt install -y gcc-multilib ia32-libs
	fi

	# Sublime merge
	if [[ 'x86_64' == $(uname -m) ]]; then
		wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
		echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
		sudo apt-get update
		sudo apt-get install sublime-merge
	fi

	# Oracle JRE 8
	sudo add-apt-repository -y ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get install -y -qq oracle-java8-installer

	echo -e "${green}Installation Outils dev Ok${nc}"
}


# Installation outils dev buildroot
function intall_dev_buildroot()
{
	echo ""
	echo -e "${green}Installation Outils dev Buildroot ...${nc}"

	sudo apt install -y \
		build-essential \
		git \
		libncurses5-dev \
		dialog \
		expect \
		flex \
		bison \
		rsync \
		expect \
		cpio \
		bc \
		pandoc \
		doxygen-latex

	echo -e "${green}Installation Outils dev Buildroot Ok${nc}"
}


# Installation outils dev pccn
function intall_dev_pccn()
{
	echo ""
	echo -e "${green}Installation Outils dev PCCN ...${nc}"

	sudo apt install -y \
		subversion \
		python-wxtools \
		meson \
		wine-development

	# check 32 ou 64 bits
	if [[ 'x86_64' == $(uname -m) ]]; then
		sudo apt install -y wine32-development
	fi

	echo -e "${green}Installation Outils dev PCCN Ok${nc}"
}


# Installation outils dev electre
function intall_dev_electre()
{
	echo ""
	echo -e "${green}Installation Outils dev Electre ...${nc}"

	sudo apt install -y \
		scons \
		python-pip \
		python-tk \
		wine-development

	# check 32 ou 64 bits
	if [[ 'x86_64' == $(uname -m) ]]; then
		sudo apt install -y wine32-development
	fi
	
	echo -e "${green}Installation Outils dev Electre Ok\n${nc}"

}


# Installation RabbitVCS et nemo comme
# gestionnaire de fichiers
function intall_rabbitvcs()
{
	echo ""
	echo -e "${green}Installation de Rabbit VCS ...${nc}"
	echo ""
	echo -e "${yellow}Pour que RabbitVCS soit correctement fonctionnel, installation de nemo comme gestionnaire de fichiers${nc}"

	sudo apt install -y \
		nemo \
		nemo-fileroller \
		nemo-python \
		rabbitvcs-cli

	# Installation de la derniere version de rabbit par dessus
	mkdir -p ~/Documents/Tools
	pushd  ~/Documents/Tools > /dev/null
	if [ ! -d ~/Documents/Tools/rabbitvcs ]; then
		git clone https://github.com/rabbitvcs/rabbitvcs.git
		cd rabbitvcs
	else
		cd rabbitvcs
		git pull
	fi
	sudo python setup.py install --install-layout=deb
	sudo cp clients/nemo/RabbitVCS.py /usr/share/nemo-python/extensions/
	popd

	echo -e "${green}Installation de Rabbit VCS Ok${nc}"
}

# installation d'eclise cdt
function intall_eclipse()
{
	echo ""
	echo -e "${green}Installation d'eclipse  ...${nc}"

	# check 32 ou 64 bits
	if [[ 'x86_64' == $(uname -m) ]]; then
		wget "${URL_ECLIPSE_64}" -O /tmp/eclipse.tar.gz

	else
		wget "${URL_ECLIPSE_32}" -O /tmp/eclipse.tar.gz
	fi

	mkdir -p ~/Documents/Tools/
	pushd ~/Documents/Tools/
	tar -xf /tmp/eclipse.tar.gz
	popd

	echo -e "${green}Installation d'eclipse Ok${nc}"
}


function optimisation()
{
	echo ""
	echo -e "${green}Optimisations/Ameliorations ...${nc}"

	# Désactiver swap
	if [[ ! $( cat /etc/sysctl.conf |grep swap) ]]; then
		sudo sed -i "$ a \n#Configuration swap" /etc/sysctl.conf
		sudo sed -i "$ a vm.swappiness=10" /etc/sysctl.conf
	fi

	# Suppression OpenOffice
	if [ -x "$(command -v libreoffice)" ]; then
		sudo apt remove -y libreoffice-*
	fi

	# Configuration terminator comme terminal par défaut
	gsettings set org.cinnamon.desktop.default-applications.terminal exec terminator
	gsettings set org.gnome.desktop.default-applications.terminal exec terminator

	# Ajout utilisateur au group de partage virtual box
	sudo usermod -G vboxsf -a $USER

	# Activation de la couleur dans le bash
	sed -i "/s#force_color_prompt/force_color_prompt/g" > ~/.bashrc


	echo -e "${green}Optimisations/Améliorations Ok ${nc}"
}




function question() {
	read -p "$1 ([O]ui or [N]on): "
	case $(echo ${REPLY} | tr '[A-Z]' '[a-z]') in
		o|oui) echo "oui" ;;
		*)     echo "non" ;;
	esac
}


############################################
#
#
#
#
###########################################

# Permet de demander le mot de passe au démarrage
sudo echo "" > /dev/null

echo -e "${yellow}###################################${nc}"
echo -e "${yellow}#${nc}"
echo -e "${yellow}# Script d'installation des outils${nc}"
echo -e "${yellow}# nécessaires au developpement${nc}"
echo -e "${yellow}#${nc}"
echo -e "${yellow}###################################${nc}"



if [[ "non" == $(question "Installer outils buidroot ?") ]]; then
	DEV_BUILDROOT=false
fi

if [[ "non" == $(question "Installer outils dev PCCN ?") ]]; then
	DEV_PCCN=false
fi

if [[ "non" == $(question "Installer outils dev Electre ?") ]]; then
	DEV_ELECTRE=false
fi

if [[ "oui" == $(question "Installer Rabbitvcs ? ${yellow}Ceci installera nemo comme gestionnaire de fichier${nc}") ]]; then
	RABBIT_VCS=true
fi

if [[ "oui" == $(question "Installer Eclipse CDT ?") ]]; then
	ECLIPSE=true
fi

# Mise à jour des dépots
update

# Outils de dev 
intall_outils_dev

# Dev buidlroot
if ${DEV_BUILDROOT}; then
	intall_dev_buildroot
fi

# Dev PCCN
if ${DEV_PCCN}; then
	intall_dev_pccn
fi

# Dev Electre
if ${DEV_ELECTRE}; then
	intall_dev_electre
fi

# Rabbit vcs
if ${RABBIT_VCS}; then
	intall_rabbitvcs
fi

# Eclipse
if ${ECLIPSE}; then
	intall_eclipse
fi

# Optimisation
optimisation

# Suppression des paquets obsoletes
sudo apt autoremove -y -qq

if ${RABBIT_VCS}; then
	echo -e "${yellow}*****************************************${nc}"
	echo -e "${yellow}Pour activer nemo par défaut : ${nc}"
	echo -e "${yellow} Parametres -> Applications favorites -> Utilitaires ${nc}"
	echo -e "${yellow} Gestionnaire de fichiers : selectionner 'Autre...' et entrer 'nemo' ${nc}"
	echo -e "${yellow}*****************************************${nc}"
fi

# Si git existe -> configuration
if [ -x "$(command -v git)" ]; then
	if [[ "oui" == $(question "Voulez vous configurer les informations de votre compte git ?") ]]; then
		echo ""
		read -p "Entrer votre prenom et nom : "
		git config --global user.name "${REPLY}"
		read -p "Entrer votre adresse mail : "
		git config --global user.email  "${REPLY}"
		echo -e "${yellow}Informations renseignées : ${nc}"
		echo -e "${nc}Prénom Nom : ${yellow} $(git config --global user.name)${nc}"
		echo -e "${nc}email : ${yellow} $(git config --global user.email)${nc}"
	fi
fi
